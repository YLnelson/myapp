function oClone(obj) { // Js varable clone to prevent from memory reference
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;
    if (obj instanceof Date) {  // Handle Date
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    if (obj instanceof Array) {     // Handle Array
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = oClone(obj[i]);
        }
        return copy;
    }
    if (obj instanceof Object) { // Handle Object
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = oClone(obj[attr]);
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
}
function rndColor(amt) {
	var rd=[0,1,2,3,4,5,6,7,8,9,10,11,12,13];
	for (var i=0;i<15;i++) {
		var _a=Math.floor(Math.random()*7), _v2=Math.floor(Math.random()*7+7);
		rd[_a] = [rd[_b] , rd[_b] = rd[_a]][0]; //Swap
	}
	i = Math.floor(Math.random()*14);
	while (amt + i > rd.length) rd = rd.concat(rd);
	rd.splice(0,i);
	rd.length = amt;
	return rd;
}

/*Template7.registerHelper('json_stringify', function (context) {
  return JSON.stringify(context);
});*/

Template7.registerHelper('listOils', function (k,v) {
	if (!k || !v) return;
	var o = JSLINQ(oClone(myApp.template7Data.oils))
			.Where(function(obj,i){
				obj.index = i;
				return obj[k].indexOf(v)>-1; 
			}).ToArray();
	var ret = '<ul>';
	for (var i in o) {
		ret += '<li><a href="#" data-template="oil" data-context-name="oils.' + o[i].index;
		ret += '" class="item-link item-content"><div class="item-media"><img src="img/eo/png/';
		ret += o[i].pic + '" /></div><div class="item-inner"><div class="item-title-row">';
		ret += '<div class="item-title">' + o[i].name + '</div><div class="item-after">';
		if (o[i].app.length >0) {
			/*var*/ k = o[i].app.split('');
			for (var v in k) ret += '<img src="img/icon/' + k[v] + '.svg" height="20" />';
		}
		ret += '</div></div><div class="item-text">'+ o[i].des +'</div></div></a></li>';
	}
	return (ret +'</ul>');
});

Template7.registerHelper('colorApp', function (str,cat) {
	if (!str || (str.length == 0)) return '';

	var ret = '<div class="content-block-title" style="margin-top:10px">';
	ret += '<p class="buttons-row" style="margin:0">';
	o = [{app:'Topical',color:'yellow'},{app:'Aromatic',color:'pink'},{app:'Dietary',color:'green'}];
	if (cat === '1') cat = 'blend'; else cat = 'single';
	str = str.split('');
	for (var i in o) { // Disable the applications not listed
		if (str.indexOf(i) < 0) ret += '<a href="#" class="button button-round color-gray">';
		else ret += '<a data="apply,'+ i + '" id="' + cat + '" class="button button-round button-fill color-' + o[i].color + '">';
		ret += o[i].app + '</a>' ;
	} 
	return ret + '</p></div>';
});
Template7.registerHelper('colorIngr', function (str) {
	if (!str || (str.length == 0)) return '';
	str = str.split(',');
	var o = JSLINQ(oClone(myApp.template7Data.oils))
					.Where(function(o,i){return o.cat == '0';})
					.Select(function(o) {return o.name})
					.ToArray();
	var ret = '', bgColor = rndColor(str.length);
	for (var i in str) {
		if (o.indexOf(str[i]) < 0) {
			ret += '<a data="" class="button button-round color-blue">' + str[i] + '</a>&nbsp;';
		} else {
			ret += '<a data="oil,'+ o.indexOf(str[i]) + '" id="blend" ';
			ret += 'class="button button-round button-fill color-list-'+ bgColor[i] +'">' + str[i] + '</a>&nbsp;';
		}
	}
	return ret;
});

Template7.registerHelper('colorChakra', function (str,cat) {
	if (!str || (str.length == 0)) return '';
	
	str = str.split('');
	var chakra = [{"name":"Root","color":"red"},{"name":"Sacral","color":"orange"},{"name":"Solar Plexus","color":"yellow"},{"name":"Heart","color":"green"},{"name":"Throat","color":"blue"},{"name":"Third Eye","color":"indigo"},{"name":"Crown","color":"purple"}];
	var ret = '<li class="accordion-item"><a href="#" class="item-content item-link">';
	if (cat === '1') cat = 'blend'; else cat = 'single';
	ret += '<div class="item-inner"><div class="item-title">Corresponding Chakra';
	if (str.length > 1) ret += 's';
	ret += '</div></div></a><div class="accordion-item-content"><div class="content-block">';
	ret += '<span class="row" style="justify-content: initial; line-height: 34px;">';
	for (var i in str) {
		ret += '<a data="chakra,' + str[i] + '" id="' + cat + '" class="button button-round button-fill color-chakra-';
		ret += chakra[str[i]].color + '">' + chakra[str[i]].name + '</a> &nbsp;';
	}
	ret = ret.slice(0, ret.length-6);
  	return ret + '</span></div></div></li>';
});
Template7.registerHelper('colorUsage', function (str,cat) {
	if (!str || (str.length == 0)) return '';

	str = str.split(',');
	if (cat === '1') cat = 'blend'; else cat = 'single';
	var bgColor = rndColor(str.length);
	var ret ='<li class="accordion-item"><a href="#" class="item-content item-link"><div class="item-inner"><div class="item-title">Benefit';
	if (str.length > 1) ret += 's';
	ret += ' provided</div></div></a><div class="accordion-item-content"><div class="content-block">';
	ret += '<span class="row" style="justify-content:initial;margin-top:0;line-height:34px">';
	for (var i in str) {  
		ret += '<a data="use,' + str[i] + '" id="' + cat + '" class="button button-round button-fill color-list-' ;
		ret += bgColor[i] + '">' + str[i] + '</a> &nbsp;';
	}
	ret = ret.slice(0, ret.length-6);
	return ret + '</span></div></div></li>';
});

Template7.registerHelper('pocketRef', function (str) {
	if (!str || (str.length == 0)) return '';
	var o = JSLINQ(oClone(myApp.template7Data.pocket)).Where(function(obj){
			return obj.YLname == str; 
		}).ToArray();
	if (o.length==0) return '';
	if (o.length >1) console.log('Find more than 1 '+ str + ' in pocket!');
	if (o.length>=1) {
		var ret = '<div class="content-block-title"><a class="button bg-white" id="expand" style="float:right">Expand All</a>Pocket Reference<br>';
		ret += '(' + o[0].Plant + ' in page ' + o[0].Pg + ')</div>';
		ret += '<div class="list-block"><ul>'
		o = o[0].data;
		for (var p in o) {
			ret += '<li class="accordion-item"><a href="#" class="item-content item-link"><div class="item-inner"><div class="item-title">';
			ret += p + '</div></div></a><div class="accordion-item-content"><div class="content-block">';
			ret += o[p] + '</div></div></li>';
		}
		return ret + '</ul></div>';
	}
});


// Initialize app
var myApp = new Framework7({
    animateNavBackIcon: true,
    precompileTemplates: true,	// Enable templates auto precompilation
    template7Pages: true,		// Enabled pages rendering using Template7
    template7Data: {			// Specify Template7 data for pages
    	oils: null, chakra: null,
    /*	'page:oil': {},  //Apply to data-page="oil" 
        'url:projects.html':  //Will be applied for page with "projects.html" url 
        { n:'', a:0, i:['',''],projects:[{c:'',d:0},{c:'', d:0}] },*/ 
    }
});

var $$ = Dom7; //Export selectors engine
$$.getJSON('oils.json',function(i){	
	myApp.template7Data.oils = i;
	console.log('[Oils info written]');
	//$$('div#eolist').html(Template7.templates.oil_list({ qField:'cat', id_s:'0', id_b:'1' }));
	$$.get('eolist.tmp',function(d){
		var tmp = Template7.compile(d);
		$$('div#eolist').html(tmp({ qField:'cat', id_s:'0', id_b:'1' }));
		console.log('[EO List created]');
	});
});
//Pocket Reference DB
$$.getJSON('Ref.json',function(i){	
	myApp.template7Data.pocket = i;
	console.log('[Pocket Info written]');
});
// Add main View
var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true,	// Enable dynamic Navbar
    domCache: true			// Enable Dom Cache so we can use all inline pages
});
//Offline notice
var app_online = function() {
	$$('.login-button').removeAttr('style');
	window.addEventListener('offline', app_offline);
	window.removeEventListener('online',function(){});
}
var app_offline = function() {
	$$('.login-button').hide();
	window.addEventListener('online', app_online);
	window.removeEventListener('offline',function(){});
}

if (!navigator.onLine) {
	myApp.alert('You\'re now offline, but you can still access part of resources','Offline Mode');
	window.addEventListener('online', app_online);
	$$('.login-button').hide();
} else window.addEventListener('offline', app_offline);
	
// Daily Reading
$$.getJSON('angels.json', function(card){
	console.log("[Calling Angel]")
	var pos = Math.floor(Math.random()*card.length); //rnd makes an int from 0-43
	card = card[pos].name + '</b></span><p>' + card[pos].text + '</p>';
	card = '.jpg" width="25%" style="float:right;margin:5px;margin-right:0px;max-width:120px;"><b>' + card;
	card = '<span style="font-size:1.5em"><img class="zoom-pic" src="img/angel/indigo_angel_cards' + String(pos+1) + card;
	$$('.content-block-inner').prepend(card);
    $$('.zoom-pic').on('click', function (e) {
   		var o = '<img src="' + this.attributes.getNamedItem("src").nodeValue + '" width="85%">';
   		var buttons = [{text: o, label: true}]
   		myApp.actions(this, buttons);
	});
});
$$.getJSON('chakra.json', function (i) {
	myApp.template7Data.chakra=i;
	console.log('[Chakra data written]');
});
console.log('[Obj:myApp Created]');

$$(document).on('pageInit', function (e) {
    var page = e.detail.page;
    if (page.name === 'oil_list'){
    	$$('a#seekBar').on('click',function(){
    		$$(this).toggleClass('seek');
    		if ($$(this).hasClass('seek')) {
    			$$('form').removeAttr('style');
    			$$('div.searchbar-overlay').removeAttr('style');
    			$$('div.page-content').scrollTo(0,0,300);
    		} else {
    			$$('form').hide();
    			$$('div.searchbar-overlay').hide();
    			$$('li.hidden-by-searchbar').removeAttr('class');
    		}
    	});
    	    	
    	/*
    	var o = JSLINQ(oClone(myApp.template7Data.oils)).Where(function(obj,i){
				obj.index = i;
				return obj.cat.indexOf('0')>-1; 
			}).ToArray();
    	var list_single = myApp.virtualList('.list-block.virtual-list.eo-blend', {
			items: JSLINQ(oClone(myApp.template7Data.oils)).Where(function(obj,i){
				obj.index = i;	return obj.cat.indexOf('0')>-1;	}).ToArray()
				, template: '<li><a href="#oil" data-context-name="oils.{{index}}" class="item-link item-content">'
			+ '<div class="item-media"><img src="img/eo/png/{{pic}}" /></div><div class="item-inner">'
			+ '<div class="item-title-row"><div class="item-title">{{name}}</div><div class="item-after">'
			+ '{{#each app}}<img src="img/icon/{{this}}.svg" height="20" />{{/each}}</div></div>'
			+ '<div class="item-text">{{des}}</div></div></a></li>' 
		});
		var o = JSLINQ(oClone(myApp.template7Data.oils)).Where(function(obj,i){
				obj.index = i;
				return obj.cat.indexOf('0')>-1; 
			}).ToArray();
		var list_blend = myApp.virtualList('.list-block.virtual-list.eo-single', {
			items: o ,	renderItem: function (i, item) {
        		var ret = '<li><a href="#" data-template="oil" data-context-name="oils.' + item.index;
				ret += '" class="item-link item-content"><div class="item-media"><img src="img/eo/png/';
				ret += item.pic + '" /></div><div class="item-inner"><div class="item-title-row">';
				ret += '<div class="item-title">' + item.name + '</div><div class="item-after">';
				if (item.app.length >0) {
					var k = item.app.split('');
					for (var v in k) ret += '<img src="img/icon/' + k[v] + '.svg" height="20" />';
				}
				ret += '</div></div><div class="item-text">'+ item.des +'</div></div></a></li>';
        		return ret;
    		}
		});
		var o = JSLINQ(oClone(myApp.template7Data.oils)).Where(function(obj,i){
				obj.index = i;
				return obj.cat.indexOf('0')>-1; 
			}).ToArray();
		
    	function oil_cat(o_index){
    		var oils = oClone(myVar.oils);
    		return JSLINQ(oils).Where(function(o,i){
				o.index = i;
				o.app = o.app.split('');
				return o.cat == o_index; 
			}).ToArray();
    	}
    	$$('#list-single').html(myTmp.oil_list(oil_cat('0')));
    	$$('#list-blends').html(myTmp.oil_list(oil_cat('1')));
    	// -- Search Bar toggle begin -- 
    	//$$('#search-on').on('click', function(){
    	//	var html = '<form class="searchbar searchbar-init" data-search-list="#content-wrap" data-search-in=".item-title" data-found=".searchbar-found" data-not-found=".searchbar-not-found"><div class="searchbar-input"><input type="search" placeholder="Search"><a href="#" class="searchbar-clear"></a></div><a href="#" class="searchbar-cancel">Cancel</a></form><div class="searchbar-overlay"></div>';
    	//	$$('.tabs-swipeable-wrap').prepend(html);
		//	this.setAttribute('id','search-off');    		
    	//});  -- Search Bar toggle end --
    	*/
    }
    if ((page.name === 'oil')||(page.name === 'oils')) {
		/* product number
		var o = obj.pno;
		if (o.length==10) {	o = o.slice(0,4) + ' - 5ml' + o.slice(4,9) + ' - 15ml';
		} else { o = o.slice(0,4) + ' - ' + o.substr(4) + '5ml'; }
		obj.pno = o.split(',');	*/
		var pro = true;
		if ((page.context.cat == '0') && pro) {
			$$('a#expand').on('click',function(){
				if ($$(this).text() == 'Expand All') {
					$$(this).parent().next().children().children().addClass('accordion-item-expanded');
					$$(this).text('Close All');
				} else {
					$$(this).parent().next().children().children().removeClass('accordion-item-expanded');
					$$(this).text('Expand All');
				}
				
				
			});
		}
		// Bind event listener to action sheets
		$$('.zoom-pic').on('click', function (e) {
   			var target = this; // zoom picture
   			var o = '<img src="' + this.attributes.getNamedItem("src").nodeValue + '" size="90%">';
   			var buttons = [{text: o, label: true}]
   			myApp.actions(target, buttons);
		});
		var p = '#';
		if (page.name === 'oil') p += 'blend'; else p += 'single';
	 	$$(p+'.button-fill').on('click', function (e) {
    		var o = this.attributes.getNamedItem("data").nodeValue;
    		var dest = {template: Template7.templates.useoil, context: {}}, target = this;
    		dest.context.qField = o.split(',')[0].slice(0,3);
    		var m = 'Explore more oils ';
    		switch (o.split(',')[0]) {
    			case "apply":
    				dest.context.id = o.split(',')[1];
    				dest.context.title = 'For ' + this.text + ' Use';
    				m += 'can be ' +this.text+ ' used as well'; break;
    			case "use":
    				dest.context.title = o.split(',')[1];
    				dest.context.id = o.split(',')[1];
    	  			m += 'that also benifits in '+this.text; break;
    	  		case "chakra":
    	  			dest.template = Template7.templates.chakra;
    	  			dest.context = myApp.template7Data.chakra[o.split(',')[1]];  //replace context
    	  			dest.context.qField = 'cha' ;
    	  			dest.context.id = o.split(',')[1];
    	  			m += 'that coresponsding to '+this.text+' Chakra'; break;
    	  		case "oil":
    	  			m = 'Take a look at ' + this.text + ' Essential Oil';
    	  			dest.template = Template7.templates.oil;
    	  			dest.context = myApp.template7Data.oils[o.split(',')[1]];  //replace context
    		}
	   		// create button and bind ajax when take me there is clicked
	   		var btn_go = [{text: m +'?',label: true},{ text:'Take me there',
          						onClick: function(){ mainView.router.load(dest); }
          						}];
		    var o = [{text:'Cancel',color:'red'}];
    		o = [btn_go, o];
    		myApp.actions(this, o);
		});
    }
    /*if (page.name === 'chakra') {
       	var o = JSLINQ(oClone(myVar.oils)).Where(function(o,i){
				o.index = i;
				return o.cha.indexOf(page.context.id)>-1; 
			}).ToArray();
    	$$('#chakraInfo').append(myTmp.oil_list(o)); //oil selection
    }
    if (page.name === 'apply') {
    	var o = ['Topical','Aromatic','Dietary'];
        $$('div#apply-title').text('Oils for '+o[page.query.id]+' Use');
        myApp.sizeNavbars();
       	o = JSLINQ(oClone(myVar.oils))
			.Where(function(obj,i){
				obj.index = i;
				return obj.app.indexOf(page.query.id)>-1; 
			})
			.ToArray();
		//console.log(o);
		$$('#applyInfo').html(myTmp.oil_list(o));
	}
    if (page.name === 'useoil') {
    	if (page.query == 'undefined') {
    		page.query = oClone(page.context);
    	    console.log(page.query);
    	    console.log('cloned');
    	}
       	var o = JSLINQ(oClone(myVar.oils))
					.Where(function(obj,i){
						obj.index = i;
						return obj[page.context.qField].indexOf(page.context.id)>-1; 
					}).ToArray();
		$$('#useoil').html(myTmp.oil_list(o));
    }*/
    
});

var tool = function (t, e, n) {
    "use strict";
    (function(t) {
        t.module("ab-base64", []).constant("base64", function() {
            var t = {
                alphabet: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
                lookup: null ,
                ie: /MSIE /.test(navigator.userAgent),
                ieo: /MSIE [67]/.test(navigator.userAgent),
                encode: function(e) {
                    var n, r, i, o, s = t.toUtf8(e), a = -1, u = s.length, c = [, , , ];
                    if (t.ie) {
                        for (n = []; ++a < u; )
                            r = s[a],
                            i = s[++a],
                            c[0] = r >> 2,
                            c[1] = (3 & r) << 4 | i >> 4,
                            isNaN(i) ? c[2] = c[3] = 64 : (o = s[++a],
                            c[2] = (15 & i) << 2 | o >> 6,
                            c[3] = isNaN(o) ? 64 : 63 & o),
                            n.push(t.alphabet.charAt(c[0]), t.alphabet.charAt(c[1]), t.alphabet.charAt(c[2]), t.alphabet.charAt(c[3]));
                        return n.join("")
                    }
                    for (n = ""; ++a < u; )
                        r = s[a],
                        i = s[++a],
                        c[0] = r >> 2,
                        c[1] = (3 & r) << 4 | i >> 4,
                        isNaN(i) ? c[2] = c[3] = 64 : (o = s[++a],
                        c[2] = (15 & i) << 2 | o >> 6,
                        c[3] = isNaN(o) ? 64 : 63 & o),
                        n += t.alphabet[c[0]] + t.alphabet[c[1]] + t.alphabet[c[2]] + t.alphabet[c[3]];
                    return n
                },
                decode: function(e) {
                    if (e = e.replace(/\s/g, ""),
                    e.length % 4)
                        throw new Error("InvalidLengthError: decode failed: The string to be decoded is not the correct length for a base64 encoded string.");
                    if (/[^A-Za-z0-9+\/=\s]/g.test(e))
                        throw new Error("InvalidCharacterError: decode failed: The string contains characters invalid in a base64 encoded string.");
                    var n, r = t.fromUtf8(e), i = 0, o = r.length;
                    if (t.ieo) {
                        for (n = []; i < o; )
                            r[i] < 128 ? n.push(String.fromCharCode(r[i++])) : r[i] > 191 && r[i] < 224 ? n.push(String.fromCharCode((31 & r[i++]) << 6 | 63 & r[i++])) : n.push(String.fromCharCode((15 & r[i++]) << 12 | (63 & r[i++]) << 6 | 63 & r[i++]));
                        return n.join("")
                    }
                    for (n = ""; i < o; )
                        n += r[i] < 128 ? String.fromCharCode(r[i++]) : r[i] > 191 && r[i] < 224 ? String.fromCharCode((31 & r[i++]) << 6 | 63 & r[i++]) : String.fromCharCode((15 & r[i++]) << 12 | (63 & r[i++]) << 6 | 63 & r[i++]);
                    return n
                },
                toUtf8: function(t) {
                    var e, n = -1, r = t.length, i = [];
                    if (/^[\x00-\x7f]*$/.test(t))
                        for (; ++n < r; )
                            i.push(t.charCodeAt(n));
                    else
                        for (; ++n < r; )
                            e = t.charCodeAt(n),
                            e < 128 ? i.push(e) : e < 2048 ? i.push(e >> 6 | 192, 63 & e | 128) : i.push(e >> 12 | 224, e >> 6 & 63 | 128, 63 & e | 128);
                    return i
                },
                fromUtf8: function(e) {
                    var n, r = -1, i = [], o = [, , , ];
                    if (!t.lookup) {
                        for (n = t.alphabet.length,
                        t.lookup = {}; ++r < n; )
                            t.lookup[t.alphabet.charAt(r)] = r;
                        r = -1
                    }
                    for (n = e.length; ++r < n && (o[0] = t.lookup[e.charAt(r)],
                    o[1] = t.lookup[e.charAt(++r)],
                    i.push(o[0] << 2 | o[1] >> 4),
                    o[2] = t.lookup[e.charAt(++r)],
                    64 !== o[2]) && (i.push((15 & o[1]) << 4 | o[2] >> 2),
                    o[3] = t.lookup[e.charAt(++r)],
                    64 !== o[3]); )
                        i.push((3 & o[2]) << 6 | o[3]);
                    return i
                }
            }
              , e = {
                decode: function(e) {
                    e = e.replace(/-/g, "+").replace(/_/g, "/");
                    var n = e.length % 4;
                    if (n) {
                        if (1 === n)
                            throw new Error("InvalidLengthError: Input base64url string is the wrong length to determine padding");
                        e += new Array(5 - n).join("=")
                    }
                    return t.decode(e)
                },
                encode: function(e) {
                    var n = t.encode(e);
                    return n.replace(/\+/g, "-").replace(/\//g, "_").split("=", 1)[0]
                }
            };
            return {
                decode: t.decode,
                encode: t.encode,
                urldecode: e.decode,
                urlencode: e.encode
            }
        }())
    }
    ).call(e, n(3))
}


/*$$.getJSON('login.json',function(obj){
  console.log(t.encode(obj));	
});*/
