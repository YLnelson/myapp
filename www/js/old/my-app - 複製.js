// Let's register Template7 helper so we can pass json string in links
Template7.registerHelper('json_stringify', function (context) {
    return JSON.stringify(context);
});

function clone(obj) { // Js varable clone to prevent from memory reference
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;
    if (obj instanceof Date) {  // Handle Date
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    if (obj instanceof Array) {     // Handle Array
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }
    if (obj instanceof Object) { // Handle Object
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
}

// Initialize app
var myApp = new Framework7({
    animateNavBackIcon: true,
    // Enable templates auto precompilation
    precompileTemplates: true,
    // Enabled pages rendering using Template7
    template7Pages: true,
    // Specify Template7 data for pages
    template7Data: {
        // Will be applied for page with "projects.html" url
        'url:projects.html': 
        {   name: 'John',
            age: 32,
            interests: ['swimming', 'music'],
            projects:
            [   {	title: 'Google',
                    description: 'Nice search engine'},
                {	title: 'YouTube',
                    description: 'Online video service'}
            ]
		}, 
		g_VAR:	{}
    //console.log(oil); //my Global Var
    }
});

// Export selectors engine
var $$ = Dom7;
var myVar = myApp.template7Data.g_VAR;
$$.getJSON('oils.json', function(json) {
   myVar.oil = json;
});

$$(document).on('pageInit', function (e) {
    var page = e.detail.page;
    if (page.name === 'oil_list') {
    	function changeapp(str) {
				var t = ['topical','aromatic','dietary'];
				str = str.split('');
				for (var i in str) str[i]=t[str[i]];
				return str;
		}
        $$.get('oil_list.tmp',{},function(template) {
			var compiledTemplate = Template7.compile(template);
			var json = clone(myVar.oil);
			json.length = 50; //load first 25 oils
			for (var j in json) {
				if (json[j].app.length>0) json[j].app = changeapp(json[j].app);
				else json[j].app = null;
			}
			//console.log(json);
			$$('#content-wrap').html(compiledTemplate(json));
		});  //add on infinite scroll listener;
		var loading = false;
		$$('.infinite-scroll').on('infinite', function () {
  			if (loading) return;	// Exit, if loading in progress
			loading = true;   // Set loading flag
			setTimeout(function () {	// Emulate 1s loading
				loading = false;	// Reset loading flag
				var json = clone(myVar.oil);
				var maxItems = json.length; //Total counts
				var lastIndex = $$('.list-block.eo-list li').length;
				var itemsPerLoad = 30;
				if (lastIndex >= maxItems) {
					// Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
					myApp.detachInfiniteScroll($$('.infinite-scroll'));
					$$('.infinite-scroll-preloader').remove();	// Remove preloader
					return;
				}	// Generate new items HTML
				json.length = lastIndex + itemsPerLoad;
				json.splice(0,lastIndex);  //removed rendered data
				var html = '';
    			for (var i = 0; i < itemsPerLoad; i++) {	
    				if (lastIndex+i == maxItems) {
    					$$('.content-block.finish-block div').text('That\'s the entire catalog');
    					break;
    				}
    				if (json[i].app.length>0) json[i].app = changeapp(json[i].app);
      				html += '<li><a href="oil.html?id=' + (lastIndex+i) + '" class="item-link item-content">'; 
					html += '<div class="item-media"><img src="img/eo/png/' + json[i].pic + '" /></div>';
					html += '<div class="item-inner"><div class="item-title-row"><div class="item-title">';
					html += json[i].name + '</div><div class="item-after">';
					for (var j in json[i].app) html += '<img src="img/icon/' + json[i].app[j] + '.svg" height="20">';
					html += '</div></div><div class="item-text">' + json[i].des + '</div></div></a></li>';
    			}
 				$$('.list-block.eo-list ul').append(html);	// Append new items
 				lastIndex = $$('.list-block.eo-list li').length;    // Update last loaded index
 			}, 1000);
		});
	}
    
    if (page.name === 'oil') {
        var index = page.query.id;
        $$.getJSON('oils.json', function(obj){
        	obj = obj[index];	
        	$$('div#oil-title').html(obj.name+' Essential Oil');
        	$$.get('oil.tmp',{},function(template){
        		var compiledTemplate = Template7.compile(template);
        		// object literation begin
        		if (obj.app.length > 0) { // default application bar: all on
					var o = [{api: 'Topical', color: 'yellow', type: ' button-fill'},{api: 'Aromatic', color: 'pink', type: ' button-fill'},{api: 'Dietary', color: 'green', type: ' button-fill'}] ;
					for (var i in o) { // Disable the applications not listed
				  		if (obj.app.indexOf(i) < 0) {
				  			o[i].color = 'gray'; o[i].type = ''; } //button will not be filled
				  	} obj.app = o;
				} else obj.app = null;
				var o = obj.pno; // product number
				if (o.length==10) {
					o = o.slice(0,4) + ' - 5ml' + o.slice(4,9) + ' - 15ml';
				} else {
					o = o.slice(0,4) + ' - ' + o.substr(4) + '5ml';
				}
				obj.pno = o.split(',');
				if (obj.use.length > 0)	{ 
					obj.use = obj.use.split(','); // usage
					var rd = [];
					for (var i=0; i <14; i++) rd.push(i);
					for (var i=1; i <30; i++) {
						var a= Math.floor(Math.random()*7);
						var b= Math.floor(Math.random()*7+7);
					    var temp = rd[a]; rd[a] = rd[b]; rd[b] = temp;
					}
					var a = Math.floor(Math.random()*14);
					for (var i in obj.use){
						var temp = {usage: obj.use[i], bg: rd[a+Number(i)]};
						if (i+a == rd.length-1) a= -1-i;
						obj.use[i] = temp;
					}
				}
				else obj.use = null;
				if (obj.cha.length > 0) {  // chakras
					var o = chakra_g;
					var m = obj.cha.split(',');
					for (var i in m) {
						o[m[i]-1].pos = m[i];
						m[i]=o[m[i]-1];
					}
					obj.cha = m;				
				} else obj.cha = null; 
				// Literation done, now render content and place into wrapper
				$$('#oil-content-wrap').html(compiledTemplate(obj));
				// Bind event listener to action sheets
				$$('.zoom-pic').on('click', function (e) {
   					var target = this; // zoon picture
   					var o = '<img src="' + this.attributes.getNamedItem("src").nodeValue + '">';
   					var buttons = [{text: o, label: true}]
   					myApp.actions(target, buttons);
				});
	 			$$('.button-fill').on('click', function (e) {
    	  			var target = this; // confirmation before ajax
    				var o = this.attributes.getNamedItem("data").nodeValue;
    				var m = 'Explore more oils ';
    				switch (o.split(',')[0]) {
    					case "apply":
    	  					m += 'can be '+this.text+' used as well'; break;
    					case "usage":
    	  					m += 'that also benifits in '+this.text; break;
    	  				case "chakra":
    	  					m += 'that coresponsding to '+this.text+' Chakra';
    				} // make url below
    				dest = o.split(',')[0] + '.html?id=' + o.split(',')[1];
	   				// create button and bind ajax when take me there is clicked
	   				var button_go = [
        						{text: m +'?',label: true},{ text:'Take me there',
            						onClick: function() {
            							mainView.loadPage(dest); //destination
            						} 
	            				}]; // //ajax to the page when it's done'
		    		var o = [ {text: 'Cancel', color: 'red'} ];
    				var o = [button_go, o];
    				myApp.actions(target, o);
				});	// action sheet end
        	});
        });
    }
    if (page.name === 'chakra') {
    	$$.getJSON('oils.json', function(obj){
    		var arr = chakra_g;
    		o = arr[page.query.id-1];
        	$$('div#chakra-title').html(o.name+' Chakra');
        	function changeapp(str) {
				var t = ['topical','aromatic','dietary'];
				str = str.split('');
				for (var j in str) str[j]=t[str[j]];
				return str;
			}
        	var o = [];
        	for (var i in obj){
        		if (obj[i].cha.indexOf(String(page.query.id)) < 0) continue;
        		o.push(obj[i]);
        		o[o.length-1].pno = i;
        		if (o[o.length-1].app.length>0)	o[o.length-1].app=changeapp(o[o.length-1].app);
				else o[o.length-1].app = null;
        	}
        	obj = { oil: o, info: {intro: arr[page.query.id-1].intro, name: arr[page.query.id-1].name }};
        	
        	$$.get('chakra.tmp',{},function(template){
        		var compiledTemplate = Template7.compile(template);
        		$$('#chakra-content-wrap').html(compiledTemplate(obj));
        	});
       });
    }
    if (page.name === 'usage') {
    	var arr = JSON.parse(JSON.stringify(myVar.oil)); //avoid memory reference
    	var	o = page.query.id;
    		    		
        $$('div#usage-title').html(o);
       	var mySQL = JSLINQ(arr)
			.Where(function(obj,i){
				obj.index = i;
				return obj.use.indexOf(o)> -1; 
			})
			.Select(function(obj) {return obj.index;})
			.ToArray();
		
		$$.each(mySQL, function(k,v){
			mySQL[k] = arr[v];
			mySQL[k].pno = v;
			if ((mySQL[k].app.length>0) && (typeof mySQL[k].app == 'string')) {
				mySQL[k].app = mySQL[k].app.split('');
				$$.each(mySQL[k].app, function(l,w) {
					o = ['Topical','Aromatic','Dietary'];
					mySQL[k].app[l] = o[w];
				});
			}
		});
		arr = {info: "", oil: mySQL};
		console.log(arr);
		//.Select(function(o,i){ return o.index = i;
        	/*var o = [];
        	for (var i in obj){
        		if (obj[i].cha.indexOf(String(page.query.id)) < 0) continue;
        		o.push(obj[i]);
        		o[o.length-1].pno = i;
        		if (o[o.length-1].app.length>0)	o[o.length-1].app=changeapp(o[o.length-1].app);
				else o[o.length-1].app = null;
        	}
        	obj = { oil: o, info: {intro: arr[page.query.id-1].intro, name: arr[page.query.id-1].name }};
        	//obj.info.intro = arr[page.query.id-1].name+' Chakra\'s color is '+arr[page.query.id-1].color ; 
        	*/
        $$.get('usage.tmp',{},function(template){
        	var compiledTemplate = Template7.compile(template);
        	$$('#usage-content-wrap').html(compiledTemplate(arr));
        });
    }

});

// Infitnite scroll


// Infitnite scroll finish


// Daily Reading
$$.getJSON('angels.json', function(angels){
	var pos = Math.floor(Math.random()*angels.length)+1;
	var message = angels[pos-1];
	var angels = '<img class="zoom-pic" src="img/angel/indigo_angel_cards' + String(pos) + '.jpg" width="90" style="float: right; margin: 5px; margin-right: 0px;">'
	angels += '<b>'+ message.name +'</b>';
	$$('span#message').html(angels);
    angels = message.text ;
    $$('p#message').html(angels);
    $$('.zoom-pic').on('click', function (e) {
   		var target = this; // zoon picture
   		var o = '<img src="' + this.attributes.getNamedItem("src").nodeValue + '">';
   		var buttons = [{text: o, label: true}]
   		myApp.actions(target, buttons);
	});
});

var chakra_g = [];
$$.getJSON('chakra.json', function (json) {
	chakra_g = json;
});
// Add main View
var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true,     // Enable dynamic Navbar
});
