function oClone(obj) { // Js varable clone to prevent from memory reference
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;
    if (obj instanceof Date) {  // Handle Date
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    if (obj instanceof Array) {     // Handle Array
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = oClone(obj[i]);
        }
        return copy;
    }
    if (obj instanceof Object) { // Handle Object
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = oClone(obj[attr]);
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
}

function rndColor(amt) {
	var rd=[0,1,2,3,4,5,6,7,8,9,10,11,12,13];
	for (var i=0;i<15;i++) {
		var a=Math.floor(Math.random()*7), b=Math.floor(Math.random()*7+7);
		var tmpV; tmpV=rd[a]; rd[a]=rd[b]; rd[b]=tmpV;
	}
	i = Math.floor(Math.random()*14);
	while (amt + i > rd.length) rd = rd.concat(rd);
	rd.splice(0,i);
	rd.length = amt;
	return rd;
}

//Network Status functions
/*var app_online = function() {
	$$('.login-button').removeAttr('style');
	window.addEventListener('offline', app_offline);
	window.removeEventListener('online',function(){});
}
var app_offline = function() {
	$$('.login-button').hide();
	window.addEventListener('online', app_online);
	window.removeEventListener('offline',function(){});
}*/

/*Template7.registerHelper('json_stringify', function (context) {
  return JSON.stringify(context);
});*/
var favList = function () {
	var strArr = JSON.parse(myApp.template7Data.favItems);
	strArr.sort(function(a,b){return (a-b)});
	for (var i in strArr) {
		var j; j = strArr[i]; 
		strArr[i] = myApp.template7Data.oils[strArr[i]];
		strArr[i]['index'] = j;
	}
	return (strArr);
};
Template7.registerHelper('transform', function (k,v) {
	if (!k) return ;
	switch (k) {
		case 'app': if (v === '') break; else return (v.replace('0','Topical / ').replace('1','Aromatic / ').replace('2','Dietary ').replace(/\/\s$/,'').concat('used')); break;
		case 'cha': 
		var str=[]; str.length=7; str.fill('<img src="img/icon/s');
		for (var i in str) {
			if (v.indexOf(i) > -1) str[i] += i +'.svg" class="chakra" />';
			else str[i] += i +'.svg" class="chakra gray" />';
		} return (str.join('')); break;
		case 'use': return(v.replace(/\,/g,', '));
	}
	return ('');
});
Template7.registerHelper('listOils', function (k,v) {
	if (!k || !v) return;
	var o = JSLINQ(oClone(myApp.template7Data.oils))
			.Where(function(obj,i){
				obj.index = i;
				return obj[k].indexOf(v)>-1; 
			}).ToArray();
	var ret = '<ul>';
	for (var i in o) {
		ret += '<li class="swipeout"><div class="swipeout-content">';
		ret += '<a href="#" data-template="oil" data-context-name="oils.' + o[i].index;
		ret += '" class="item-link item-content"><div class="item-media"><img src="img/eo/png/';
		ret += o[i].pic + '" /></div><div class="item-inner"><div class="item-title-row">';
		ret += '<div class="item-title">' + o[i].name + '</div><div class="item-after">';
		if (o[i].app.length >0) {
			k = o[i].app.split('');
			for (var v in k) ret += '<img src="img/icon/' + k[v] + '.svg" height="20" />';
		}
		ret += '</div></div><div class="item-text">'+ o[i].des +'</div></div></a></div>';
		ret += '<div class="swipeout-actions-right" seq="'+o[i].index+'"><a class="info-button bg-orange">Info</a>';
		ret += '<a class="fav-button bg-pink">favorite_border</a></div></li>';
	}
	return (ret +'</ul>');
});

Template7.registerHelper('colorApp', function (str,cat) {
	if (!str || (str.length == 0)) return '';
	var ret = '<div class="content-block-title" style="margin-top:10px">';
	ret += '<p class="buttons-row" style="margin:0">';
	o = [{app:'Topical',color:'yellow'},{app:'Aromatic',color:'pink'},{app:'Dietary',color:'green'}];
	if (cat === '1') cat = 'blend'; else cat = 'single';
	str = str.split('');
	for (var i in o) { // Disable the applications not listed
		if (str.indexOf(i) < 0) ret += '<a href="#" class="button button-round color-gray">';
		else ret += '<a data="apply,'+ i + '" id="' + cat + '" class="button button-round button-fill color-' + o[i].color + '">';
		ret += o[i].app + '</a>' ;
	} 
	return ret + '</p></div>';
});

Template7.registerHelper('colorIngr', function (str) {
	if (!str || (str.length == 0)) return '';
	str = str.split(',');
	var o = JSLINQ(oClone(myApp.template7Data.oils))
					.Where(function(o,i){return o.cat == '0';})
					.Select(function(o) {return o.name})
					.ToArray();
	var ret = '', bgColor = rndColor(str.length);
	for (var i in str) {
		if (o.indexOf(str[i]) < 0) {
			ret += '<a data="" class="button button-round color-blue">' + str[i] + '</a>&nbsp;';
		} else {
			ret += '<a data="oil,'+ o.indexOf(str[i]) + '" id="blend" ';
			ret += 'class="button button-round button-fill color-list-'+ bgColor[i] +'">' + str[i] + '</a>&nbsp;';
		}
	}
	return ret;
});

Template7.registerHelper('colorChakra', function (str,cat) {
	if (!str || (str.length == 0)) return '';
	
	str = str.split('');
	var chakra = [{"name":"Root","color":"red"},{"name":"Sacral","color":"orange"},{"name":"Solar Plexus","color":"yellow"},{"name":"Heart","color":"green"},{"name":"Throat","color":"blue"},{"name":"Third Eye","color":"indigo"},{"name":"Crown","color":"purple"}];
	var ret = '<li class="accordion-item"><a href="#" class="item-content item-link">';
	if (cat === '1') cat = 'blend'; else cat = 'single';
	ret += '<div class="item-inner"><div class="item-title">Corresponding Chakra';
	if (str.length > 1) ret += 's';
	ret += '</div></div></a><div class="accordion-item-content"><div class="content-block">';
	ret += '<span class="row" style="justify-content: initial; line-height: 34px;">';
	for (var i in str) {
		ret += '<a data="chakra,' + str[i] + '" id="' + cat + '" class="button button-round button-fill color-chakra-';
		ret += chakra[str[i]].color + '">' + chakra[str[i]].name + '</a> &nbsp;';
	}
	ret = ret.slice(0, ret.length-6);
  	return ret + '</span></div></div></li>';
});

Template7.registerHelper('colorUsage', function (str,cat) {
	if (!str || (str.length == 0)) return '';

	str = str.split(',');
	if (cat === '1') cat = 'blend'; else cat = 'single';
	var bgColor = rndColor(str.length);
	var ret ='<li class="accordion-item"><a href="#" class="item-content item-link"><div class="item-inner"><div class="item-title">Benefit';
	if (str.length > 1) ret += 's';
	ret += ' provided</div></div></a><div class="accordion-item-content"><div class="content-block">';
	ret += '<span class="row" style="justify-content:initial;margin-top:0;line-height:34px">';
	for (var i in str) {  
		ret += '<a data="use,' + str[i] + '" id="' + cat + '" class="button button-round button-fill color-list-' ;
		ret += bgColor[i] + '">' + str[i] + '</a> &nbsp;';
	}
	ret = ret.slice(0, ret.length-6);
	return ret + '</span></div></div></li>';
});

Template7.registerHelper('pocketRef', function (str, cat) {
	if (!str || (str.length == 0)) return '';
	var o = JSLINQ(oClone(myApp.template7Data.pocket)).Where(function(obj){
			return obj.YLname == str; 
		}).ToArray();
	if (o.length==0) return '';
	if (o.length >1) console.log('Find more than 1 '+ str + ' in pocket!');
	if (o.length>=1) {
		var ret = '<div class="content-block-title"><a class="button bg-white" id="expand" style="float:right">Expand</a>Essential Oil Pocket<br>Reference Page ';
		ret += o[0].Pg + '</div><div class="list-block"><ul>'
		o = o[0].data;
		if ( Number(cat) < 1) {
			for (var p in o) {
				ret += '<li class="accordion-item"><a href="#" class="item-content item-link"><div class="item-inner"><div class="item-title">';
				ret += p + '</div></div></a><div class="accordion-item-content"><div class="content-block">';
				ret += o[p] + '</div></div></li>';
			}  // FOr Single Oil
		} else {
			var i;
			for (var p in o) {
				ret += '<li class="accordion-item"><a href="#" class="item-content item-link"><div class="item-inner"><div class="item-title">';
				ret += p + '</div></div></a><div class="accordion-item-content">';
				if ( (typeof o[p] === 'object') && !Array.isArray(o[p]) ) { //if o[p] is object create inset
					ret += '<div class="list-block inset" style="margin-left: 35px;margin-right: 0px;"><ul>';
					for (i in o[p]) {
						ret += '<li class="accordion-item"><a href="#" class="item-content item-link"><div class="item-inner"><div class="item-title">';
						ret += i + '</div></div></a><div class="accordion-item-content"><div class="content-block">' + o[p][i] + '</div></div></li>';
					}
					ret += '</ul></div></div></li>';
				} else { //if o[p] is array need to join
					i = (typeof o[p] === 'string')? o[p] : o[p].join('</p><p>') ;  
					ret += '<div class="content-block"><p>' + i + '</p></div></div></li>';
				}
			}
		}
		return ret + '</ul></div>';
	}
});
/*
var API = function (args) {
	var $ = this;
	$.args = {useIDB: true, useLS: false,useFB: false };
	for (var _i in args) $.args[_i] = args[_i];
	
	var idb = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
	if (!idb) $.args.useLS = true,	$.args.useIDB = false, $DB =window.localStorage;
	else $.DB = ($.args.useLS)?  window.localStorage: idb;
	
	$.wait = function (fn, cb) {
    	var count = fn.length;
    	fn.forEach(function(f) { f(next) });
    function next(r) {
        count--;
        if(count === 0) cb(result);
    }

};
*/
// Initialize app
console.log('[Initialzing App]');
var myApp = new Framework7({ animateNavBackIcon: true,
    precompileTemplates: true, template7Pages: true, sortable: false,
    template7Data: 
    { oils: null, chakra: null, pocket: null, price: null, favItems: [], switchData: favList,
    event : [ 
    {'dd':26,'mm':'JAN','yy':2017,'eventName':'Chakra Meditation with Essential Oils','city':'Manila','country':'PH','eventLocation':'XXStudio','host':'Harris Surya Jahim'},
    {'dd':26,'mm':'JAN','yy':2017,'eventName':'Chakra Meditation with Essential Oils','city':'Manila','country':'PH','eventLocation':'XXStudio','host':'Harris Surya Jahim'},
    {'dd':26,'mm':'JAN','yy':2017,'eventName':'Chakra Meditation with Essential Oils','city':'Manila','country':'PH','eventLocation':'XXStudio','host':'Harris Surya Jahim'}]},
    
    
});
console.log('[Initialzing App - Completed]');

myApp.getDB = function () {
	console.log('Start loading');
	myApp.DBset = undefined;
	window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
	window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
	window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
	
	var db, _e = function (e) { console.log ("Error at " + e.timeStamp + ":" + e.target.errorCode) }
	var req = window.indexedDB.webkitGetDatabaseNames();
	req.onerror = _e; req.onsuccess= function (e) {
		console.log('Complete checking :' + e.timeStamp);
		console.log(e);
		req = e.target.result;
		if (req.length > 0)  return ;
		//ajax data //upgrade need Load data
	};
	
	console.log('Start opening :');
	var req = indexedDB.open("appDB",1);	req.onerror = _e;
	//req.onupgrade needed download and dispatch
	req.onsuccess = function (e) { 
		var db = e.target.result;
		db.transaction("appDB").objectStore("appDB").openCursor().onsuccess = function(e) {
			var cursor = e.target.result;
			if (cursor) {
				var _cur = cursor.value; console.log('Unpacking '+ _cur.table + ':' + e.timeStamp);
				if ( _cur.location == 'ls') myApp.ls.setItem( _cur.table, JSON.stringify( _cur.data ));
	  			else myApp.template7Data[ _cur.table] = _cur.data;
				cursor.continue();
			}
		};
	};
	myApp.DBset = true;
};

var $$ = Dom7; //Export selectors engine
var useFB,useDB;
if (useDB) myApp.getDB();

if (useFB) {
	firebase.initializeApp({
	    apiKey: "AIzaSyA65v6UI271nvuENUkGjlweSnonV9ISoq8",
	    authDomain: "testdb-f0fc9.firebaseapp.com",
	    databaseURL: "https://testdb-f0fc9.firebaseio.com",
	    storageBucket: "testdb-f0fc9.appspot.com",
	    messagingSenderId: "148973132612"
	});	
} else {
	firebase = undefined;
}

// Getting Data for LocalStorage
// Setting
// Membership
function mbrData() {
	for (var i in myApp.ls) if (i.slice(0,17) === 'firebase:authUser')	return JSON.parse(myApp.ls.getItem(i));
}
mbr = mbrData();
if (mbr) {
	var i = $$('div#member');
	mbr.photoURL = "img/angel/indigo_angel_cards13.jpg";
	if (mbr.photoURL) i.find('i').hide().next().attr('src', mbr.photoURL).removeAttr('style');
	i.find('span').html(mbr.displayName ? mbr.displayName : 'Natural Lover').next().removeAttr('style');
	i.find('a').toggleClass('badge').html('Logout');
}

console.log('[Loading EO Info]');
if (!useDB) {
	$$.getJSON('oils.json', function(i){
		myApp.template7Data.oils = i;
		console.log('[Loading EO Info - Completed]');
		console.log('[Create EO list]');
		$$.get('eolist.tmp',function(d){
			var tmp = Template7.compile(d);
			$$('div#eolist').html(tmp({ qField:'cat', id_s:'0', id_b:'1' }));
			console.log('[Create EO list - Completed]');
		});
	});
} else {
	if (useDB) $$.get('eolist.tmp',function(d){
	
		function checkDB () {
			if (!myApp.DBset) {
				setTimeout(checkDB(),100);
				console.log('Gotta wait');
			}
			else return true;
		}
		var tmp = Template7.compile(d);
		console.log('');
		if (checkDB()) $$('div#eolist').html(tmp({ qField:'cat', id_s:'0', id_b:'1' }));
		console.log('[Create EO list - Completed]');
	});
}
//Pocket Reference DB
console.log('[Loading Ref Info]');
if (!useDB) $$.getJSON('Ref.json',function(i){	
	myApp.template7Data.pocket = i;
	console.log('[Loading Ref Info - Completed]');
});

if (!useDB) {
	var favItems = myApp.ls.getItem('favItems');
	if (!favItems) myApp.ls.setItem('favItems','[]');
	myApp.template7Data.favItems = myApp.ls.getItem('favItems'); 
}

// Add main View
console.log('[Adding Main View]');
var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true,	// Enable dynamic Navbar
    domCache: true			// Enable Dom Cache so we can use all inline pages
});
console.log('[Adding Main View - Completed]');

/* Offline Detection 
if (!navigator.onLine) {
	myApp.alert('You\'re now offline, but you can still access part of resources','Offline Mode');
	window.addEventListener('online', app_online);
	$$('.login-button').hide();
} else window.addEventListener('offline', app_offline);
console.log('[Online Detection - Done]');
*/

// Daily Reading
var myCard = JSON.parse(myApp.ls.getItem('IndigoCard'));
var tempFunc = function(obj) {
	var ret = '<span style="font-size:1.5em"><img class="zoom-pic" src="img/angel/'; 
	ret += obj.file + '" style="float:right;margin:5px;margin-right:0px;width:25%;height:auto;max-width:350px"><b>';
	ret += obj.name + '</b></span><p>' + obj.text + '</p>';
	$$('.content-block-inner').prepend(ret);
	$$('.zoom-pic').on('click', function (e) {
		var o = '<img src="' + this.attributes.getNamedItem("src").nodeValue + '" width="85%">';
		var buttons = [{text: o, label: true}]
		myApp.actions(this, buttons);
	});
};

if (myCard == null) var myCard = {cardDate: ''};
if (myCard.cardDate != new Date().toDateString()) {
	console.log("[Getting new daily reading]");
	$$.getJSON('angels.json', function(i){
		console.log("[Indigo card data loaded]");
		myCard = i[Math.floor(Math.random()*i.length)];
		tempFunc(myCard);
		myCard.cardDate = new Date().toDateString();
		myApp.ls.setItem('IndigoCard', JSON.stringify(myCard));
		console.log("[Indigo Card Data - Saved]");
	});
} else {
	tempFunc(myCard);
}

console.log("[Loading Chakra Data]")
if (!useDB) $$.getJSON('chakra.json', function (i) {
	myApp.template7Data.chakra = i;
	console.log("[Loading Chakra Data - Completed]")
});

// -- Virtual Office --
//$$('iframe#vo').attr('width',window.innerWidth).attr('height',window.innerHeight-$$('.navbar-inner').width());
//.attr('src','https://www.youngliving.com/vo/');

//Profile Picture Cropper
$$('div#member').find('#profile').on('click', function(){
	if (!firebase || !navigator.onLine) return;
	if (!firebase.auth().currentUser) return;
	if ($$(this).is('img')) {
		var buttons = [ 
        { text: 'Get a New Photo', onClick: function () { $$('div#member').find('input').click() } },
        { text: 'Edit Profile Picture', onClick: function () { } },
        { text: 'Cancel', color: 'red'} ];
    	myApp.actions(buttons);
	} else $$('div#member').find('input').click(); 
});

$$('div#member').find('input[type="file"]').on('change',function (){
	//myApp.closePanel(); 應該不需要
	var blobUrl = window.URL.createObjectURL($$('div#member').find('input')[0].files[0]);
	$$('div.page[data-page="cropper"]').find('img.cropper').attr('src',blobUrl);
	mainView.router.load({pageName: 'cropper', pushState: false});
});

$$('a.loadPage').on('click',function(){
    var vo = this.attributes.getNamedItem("data").nodeValue.split(',');
    var url = "http://myylapps.890m.com/load2.php?url=";
    url += 'https%3A%2F%2Fwww.youngliving.com%2F'+ vo[1] +'%2Fopportunity%2Fpromotions%2Fjanuary-promotion-2017';
    $$.get(url,{},function(r) {
    	$$.get('css/YLweb/ylweb.css',{},function(s){
		    mainView.router.loadContent(
	        '<div class="navbar">' +
	        '  <div class="navbar-inner">' +
	        '    <div class="left sliding"><a href="#" class="back link"><i class="icon icon-back"></i><span>Back</span></a></div>' +
	        '    <div class="center sliding">Latest Promotion</div>' +
	        '  </div>' +
	        '</div>' +
	        '<div class="pages">' +
	        '  <div data-page="dynamic-content" class="page">' + '<style>' + s + '</style>' +
	        '    <div class="page-content">' + r.replace(/\<img\ssrc\=\"/g,'<img src="https://www.youngliving.com') +
	        '    </div>' +
	        '  </div>' +
	        '</div>'
	    	);
		});
    });
    return;
})

$$('a#login').on('click', function(){
	if (!firebase || !navigator.onLine) return;
	
	if ($$(this).hasClass('close-panel')) {
		if (firebase.auth().currentUser) {
			firebase.auth().signOut();
			var i = $$('div#member');
			i.find('i').removeAttr('style').attr('style','font-size:100px').next().hide();
			i.find('span').html('Not yet logged in. ').next().hide();
			i.find('a').toggleClass('badge').html('Login?');	 
		} else {
			myApp.loginScreen();
		}
	} else {
		var loginId, loginPwd;
		$$('form#login').find('input').each(function(){
			if ($$(this).is('input[type="checkbox"]')) {
				if (!$$(this).prop('checked')) {
					var i = myApp.formGetData('login');
					i.pwd = '';
					myApp.formStoreData('login',i);
				} return ;
			}
			if ($$(this).attr('type') == 'text') loginId = $$(this).val();
			else loginPwd = $$(this).val();
		});
		firebase.auth().signInWithEmailAndPassword(loginId, loginPwd).then(function (currentUser){
			if (!firebase.auth().currentUser.emailVerified) console.log('Warning email not verified');
			mbr = currentUser;
			var i = $$('div#member');
			if (mbr.photoURL) i.find('i').hide().next().attr('src', mbr.photoURL).removeAttr('style');
			i.find('span').html(mbr.displayName ? mbr.displayName : 'Natural Lover').next().removeAttr('style');
			i.find('a').toggleClass('badge').html('Logout');
			myApp.closeModal('div.login-screen');
		}, function(e){
			myApp.alert(e.message, 'Error: ' + e.code.slice(5)); 
		});
	}
});



$$(document).on('pageInit', function (e) {
    var page = e.detail.page;
    console.log('[Init Page: ' + page.name +']');
    
    if (page.name === 'oil_list'){
    	var favArray = JSON.parse(myApp.template7Data.favItems);
    	//favArray.sort(function(a,b){return(a-b)});
    	$$('.swipeout-actions-right').filter(function(index,el){return favArray.indexOf(index)>-1}).children('.fav-button').html('favorite');
    	
    	$$('a#seekBar').on('click',function(){
    		$$(this).toggleClass('seek');
    		if ($$(this).hasClass('seek')) {
    			console.log($$('.page-on-center').children().children());
    			$$('form').removeAttr('style');
    			$$('div.searchbar-overlay').removeAttr('style');
    			$$('div.page-content').scrollTo(0,0,300);
    		} else {
    			$$('form').hide();
    			$$('div.searchbar-overlay').hide();
    			$$('li.hidden-by-searchbar').removeAttr('class');
    		}
    	});
    	$$('div.swipeout-actions-right').children().on('click', function(){
    		var msgHTML, action, seq = $$(this).parent().attr('seq');
    		if ($$(this).hasClass('fav-button')) {
    			var msgHTML = '<div class="modal modal-in" style="display:block;margin-top:-50px">';
				msgHTML += '<div class="modal-inner" style="color:#ffffff; background-color:rgb(99,99,99)"><div class="modal-title">';
				if ($$(this).html() === 'favorite') {
					$$(this).html('favorite_border');
					msgHTML += 'Favorite item removed';
					myApp.ls.setItem('favItems', myApp.template7Data.favItems.replace(new RegExp("([^\\d])("+String(seq)+")([^\\d])"),"$1$3").replace(/\,\,/,",").replace(/\[\,/,"[").replace(/\,\]/,"]"));
					myApp.template7Data.favItems = myApp.ls.getItem('favItems');
				} else {
					$$(this).html('favorite');
					myApp.ls.setItem('favItems', myApp.template7Data.favItems.concat(','+seq+']').replace('],',',').replace('[,','['));
					myApp.template7Data.favItems = myApp.ls.getItem('favItems');
					msgHTML += 'Favorite item added';
				}
				msgHTML += '</div><!--div class="modal-text">hello</div--></div></div>';
				msgHTML += '<div class="model model-overlay modal-overlay-visible remove-on-close" style="background:transparent"></div>';
				//update storage data for favorites
				var action = function () {
					myApp.root.append(msgHTML);
					
					window.setTimeout(function(){
						$$(myApp.root).children().eq($$(myApp.root).children().length-3).nextAll().remove();
					},1000);
    			};
			} else {
				var msgHTML = { title: myApp.template7Data.oils[seq].name + ' Essential Oil',
							text: myApp.template7Data.oils[seq].price , buttons: [{text: 'OK'}]}
				var action = function () {myApp.modal(msgHTML)} ;
			}
			myApp.swipeoutClose($$(this).parent().parent(), action);
		});
    	if (!myApp.ls.getItem('price')) {
    		if (myApp.device.ios && myApp.minimalUi && !myApp.webView) {
    		 //	can't use GAS
    		} else {
			dbUrl = 'https://script.google.com/macros/s/AKfycbzpsUoLisN8RP6h64wTp4JAwhvv9sHRk-RUtMSKFA9yTImvwew/exec';
			query = {func: 'ER_request', data: 'collect/US'};
			$$.post(dbUrl, query, function(r){
				myApp.alert(r.length);
				var d = JSON.parse(r);
				if (!d.result) return ;
				myApp.alert('Downloaded');
				d = d.data ;
				for (var i in d) {
					r = d[i].match(/[\d\.]+/g);
					if (r.length == 4) myApp.template7Data.oils[i].price = '5ml: $'+r[1]+' ('+r[0]+'PV)<br>15ml: $'+r[3]+' ('+r[2]+'PV)';
					else {
						if (d[i].indexOf('$') == 0) myApp.template7Data.oils[i].price = '15ml: $'+r[1]+' ('+r[0]+'PV)';
						else myApp.template7Data.oils[i].price = '5ml: $'+r[1]+' ('+r[0]+'PV)';
					}
				}
				var d = [];	for (var i in myApp.template7Data.oils) d.push(myApp.template7Data.oils[i].price);
				myApp.ls.setItem('price',JSON.stringify(d));
				myApp.alert(myApp.ls.getItem('price').length);
			});	}
		} else {
			var d = JSON.parse(myApp.ls.getItem('price'));
			for (var i in d) myApp.template7Data.oils[i].price = d[i];
		}
    }
    if (page.name === 'cropper') {
    	function setting() {
    		var _viewport = {type: 'circle'}, _boundry = {};
    		_viewport.width = ($$('div.views').width())? Math.floor($$('div.views').width() *0.8) : 300;
    		_viewport.height = ($$('div.views').height())? Math.floor($$('div.views').height() *0.8) : 534;
    		_boundry.width = _viewport.width *1.6; _boundry.height = _viewport.height *1.6;
    		if ( _viewport.width > _viewport.height) _viewport.width = _viewport.height;
    		else _viewport.height = _viewport.width;
    		return {viewport: _viewport, boundry: _boundry, showZoomer: false, enableOrientation: true};
    	}
		
    	var canvas = new Croppie($$(this).find('img.cropper')[0], setting()) , canvasSent=false;
		$$('div.page[data-page="cropper"]').find('a.item-link').on('click',function(){
			switch ($$(this).children().html()) {
				case 'rotate_left' : canvas.rotate(-90); break;
				case 'rotate_right' : canvas.rotate(90); break;
				case 'save' : $$(this).parent().parent().toggleClass('speed-dial-opened');
					if (canvasSent) return; else canvasSent = true;
					canvas.result({type:'blob',size:'viewport'}).then(function(blob){
						var u = window.URL.createObjectURL(blob);
						$$('div.popup').find('img#cropper').attr('src',u);
						if (u) { myApp.popup('div.popup');
						$$(document).once('popup:close', function(){
							$$('img#croppie').removeAttr('src');
							canvasSent = window.URL.revokeObjectURL(u);
							console.log(canvasSent);
							console.log('Url--');
						});
						}
					});				
			}
			
		});

		
    }
    if ((page.name === 'oil')||(page.name === 'oils')) {
		/* product number
		var o = obj.pno;
		if (o.length==10) {	o = o.slice(0,4) + ' - 5ml' + o.slice(4,9) + ' - 15ml';
		} else { o = o.slice(0,4) + ' - ' + o.substr(4) + '5ml'; }
		obj.pno = o.split(',');	*/
		var pro = true;
		//Pocket Reference Section
		if (pro) {
			$$('a#expand').on('click',function(){
				var targetEl = $$(this).parent().next().find('li');
				console.log(targetEl);
				if ($$(this).text() == 'Expand') {
					targetEl.removeClass('accordion-item-expanded');
					targetEl.addClass('accordion-item-expanded');
					$$(this).text('Collapse');
				} else {
					targetEl.addClass('accordion-item-expanded');
					targetEl.removeClass('accordion-item-expanded');
					$$(this).text('Expand');
				}
			});
		}
		
		// Bind event listener to action sheets
		$$('.zoom-pic').on('click', function (e) {
   			var target = this; // zoom picture
   			var o = '<img src="' + this.attributes.getNamedItem("src").nodeValue + '" style="width:90%;max-width:120px">';
   			var buttons = [{text: o, label: true}]
   			myApp.actions(target, buttons);
		});
		var p = '#';
		if (page.name === 'oil') p += 'blend'; else p += 'single';
	 	$$(p+'.button-fill').on('click', function (e) {
    		var o = this.attributes.getNamedItem("data").nodeValue;
    		var dest = {template: Template7.templates.useoil, context: {}}, target = this;
    		dest.context.qField = o.split(',')[0].slice(0,3);
    		var m = 'Explore more oils ';
    		switch (o.split(',')[0]) {
    			case "apply":
    				dest.context.id = o.split(',')[1];
    				dest.context.title = 'For ' + this.text + ' Use';
    				m += 'can be ' +this.text+ ' used as well'; break;
    			case "use":
    				dest.context.title = o.split(',')[1];
    				dest.context.id = o.split(',')[1];
    	  			m += 'that also benifits in '+this.text; break;
    	  		case "chakra":
    	  			dest.template = Template7.templates.chakra;
    	  			dest.context = myApp.template7Data.chakra[o.split(',')[1]];  //replace context
    	  			dest.context.qField = 'cha' ;
    	  			dest.context.id = o.split(',')[1];
    	  			m += 'that coresponsding to '+this.text+' Chakra'; break;
    	  		case "oil":
    	  			m = 'Take a look at ' + this.text + ' Essential Oil';
    	  			dest.template = Template7.templates.oil;
    	  			dest.context = myApp.template7Data.oils[o.split(',')[1]];  //replace context
    		}
	   		// create button and bind ajax when take me there is clicked
	   		var btn_go = [{text: m +'?',label: true},{ text:'Take me there',
          						onClick: function(){ mainView.router.load(dest); }
          						}];
		    var o = [{text:'Cancel',color:'red'}];
    		o = [btn_go, o];
    		myApp.actions(this, o);
		});
    }
    if (page.name == 'profile') {
    	var t; t = myApp.formToJSON('form#profile');
    	$$('a.cancelSave').on('click',function() {
    		if (JSON.stringify(t) == JSON.stringify(myApp.formToData('form#profile'))) {
    			myApp.mainView.router.back({pageName: 'index'});
    		} else {
    			myApp.confirm('Leaving without save the data?','Warning',function(){
    				myApp.formFromData('form#profile', t);
    				myApp.mainView.router.back({pageName: 'index'});
    			}, function(){});
    		} return ;
    	});
    	$$('a.Save').on('click',function() {
    		if (!navigator.onLine) console.log('No Internet');
    		if (!firebase || useFB) console.log('Firebase not ready');
    		var t; t = $$('form#profile').find('input[name="displayName"]').val();
    		if ( t == firebase.auth().currentUser.displayName ) return ;
    		firebase.auth().currentUser.updateProfile({displayName: t, photoURL: firebase.auth().currentUser.photoURL});
    		$$('div#member').find('span').html(t); 
    	})
    }
    
    if (page.name == 'events') {
    	$$('div.timeline-item-content').on('click',function(){
    		myApp.alert('Event content');
    	});
    	
    }
    if (page.name == 'favItem') {
    	$$('a.link').on('click',function(){
    		$$(this).toggleClass('pick');
    	});
    	$$('i.facebook-action').on('click', function () {
    		var clickedLink = this;
    		// in the future in case there will be more than one popover
    		// just dynamiclly generate list item before popover
    		$$('a.action-button').attr('target',$$(this).attr('target'));
			myApp.popover('.popover-action', clickedLink);
		});  	
    }
    console.log('[Page "' + page.name +'" initialized]');
});